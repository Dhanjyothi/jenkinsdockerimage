package com.hcl.webinar.service;

import java.util.List;
import java.util.Optional;
  
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.webinar.model.Customer;
import com.hcl.webinar.repository.CustomerRepository;


@Service("customerService")
@Transactional
public class CustomerService {

	@Autowired
	CustomerRepository customerRepo;

	
	public List<Customer> getAllCustomers() {
		return (List<Customer>) customerRepo.findAll();
	}

	/*@Transactional
	public Customer getCustomer(int id) {
		Optional<Customer> customer=customerRepo.findById(id);
		System.out.println("entered");
		return customer.get();
	}*/

	public void addCustomer(Customer customer) {
		customerRepo.save(customer);
	}

	/*@Transactional
	public void updateCustomer(Customer customer) {
		customerRepo.save(customer);

	}*/

	/*@Transactional
	public void deleteCustomer(int id) {
		customerRepo.deleteById(id);
	}*/
}
