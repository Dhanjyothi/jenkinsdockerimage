package com.hcl.webinar.service;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import com.hcl.webinar.model.Customer;

import org.junit.Before;
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
//@ComponentScan(basePackages = "com.dhanjyothi")
@ContextConfiguration(classes = { TestBeanConfig.class })
public class ServiceTest {

	@Autowired
	CustomerService ser;
	private Customer p;
	@Before
	public void setup() {
	
		p=new Customer();
	}
	 @Test
	   public void testGet() {
		
		p.setCustomerName("revathi");
		p.setEmail("revathi@gmail.com");
		ser.addCustomer(p);
		List<Customer> p=ser.getAllCustomers();
		System.out.println("Person"+p);
		for (Customer personObj : p) {
			System.out.println(personObj.getCustomerName());
		      assertEquals("revathi",personObj.getCustomerName() );
	    }  

	 }
}
