FROM java:8
VOLUME /tmp
EXPOSE 8081
ADD target/JenkinsSonarQubeGitDemo-0.0.1-SNAPSHOT.jar hello1-docker.jar
ENV JAVA_OPTS=""
ENTRYPOINT ["java","-jar","/hello1-docker.jar"]